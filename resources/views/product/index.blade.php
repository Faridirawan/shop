@extends('layouts.template')

@section('content')
<div class="container-fluid">
    <!-- Basic Examples -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        DATA PRODUK
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li>
                            <a href="{{route('product.create')}}">
                                <button type="button" class="btn btn-default waves-effect btn-sm"><i class="material-icons">add_circle</i></button>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Produk</th>
                                    <th>Gambar Produk</th>
                                    <th>Kategori Produk</th>
                                    <th>Deskripsi Produk</th>
                                    <th>Harga</th>
                                    <th>Pilihan</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Produk</th>
                                    <th>Gambar Produk</th>
                                    <th>Kategori Produk</th>
                                    <th>Deskripsi Produk</th>
                                    <th>Harga</th>
                                    <th>Pilihan</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($products as $i => $item)
                                    <tr class="bg-danger">
                                        <th>{{ $i+1 }}</th>
                                        <td>{{ $item->name }}</td>
                                        <td><img src="{{ URL::to('/') }}/products/gambar/{{$item->image}}" class="img-thumbnail" width="100px" height="100px"></td>
                                        <td>{{ $item->categoryRef->name }}</td>
                                        <td>{{ $item->desc }}</td>
                                        <td> Rp.{{ $item->price }}</td>
                                        <td>
                                            <form action="{{ route ('product.destroy',$item->id) }}" method="post">
                                                <a href="{{ route ('product.edit', $item->id) }}">
                                                    <button type="button" class="btn btn-success"><i class="material-icons">edit</i></button>
                                                </a>
                                                @csrf 
                                                @method('delete')
                                                    <button type="submit" class="btn btn-danger" 
                                                    onclick="return confirm('Yakin Dihapus?' )"><i class="material-icons">delete</i></button>

                                                    <a href="{{ route('product.show', $item->id) }}">
                                                        <button type="button" class="btn btn-info"><i class="material-icons">list</i></button>
                                                    </a>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Examples -->
</div>
@endsection