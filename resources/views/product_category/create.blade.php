@extends('layouts.template')

@section('content')
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Tambah Data Kategori</h2>
            </div>
            <div class="body">
                <form action="{{route('product_category.store')}}" method="POST">
                    @csrf
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" id="name" name="name" required>
                            <label class="form-label">Nama Kategori</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea name="desc" id="desc" cols="30" rows="5" class="form-control no-resize" required></textarea>
                            <label class="form-label">Deskripsi Kategori</label>
                        </div>
                    </div>
                    <button class="btn btn-danger waves-effect" type="submit">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
@endsection