@extends('layouts.template')

@section('content')
<div class="container-fluid">
    <!-- Basic Examples -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        DATA KATEGORI PRODUK
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li>
                            <a href="{{route('product_category.create')}}">
                                <button type="button" class="btn btn-default waves-effect btn-sm"><i class="material-icons">add_circle</i></button>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                	<th>No</th>
                                    <th>Nama Kategori</th>
                                    <th>Deskripsi Kategori</th>
                                    <th>Pilihan</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Kategori</th>
                                    <th>Deskripsi Kategori</th>
                                    <th>Pilihan</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($category as $i => $item)
                                <tr>
                                    <td>{{$i+1}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->desc}}</td>
                                    <td>
                                        <form action="{{ route('product_category.destroy',$item->id) }}" method="post">
                                            <a href="{{ route('product_category.edit',$item->id) }}">
                                                <button type="button" class="btn btn-success btn-sm"><i class="material-icons">edit</i></button>
                                            </a>
                                            @csrf 
                                            @method('delete')
                                                <button type="submit" class="btn btn-danger btn-sm" 
                                                onclick="return confirm('Yakin ingin dihapus ?')"><i class="material-icons">delete</i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Examples -->
</div>
@endsection