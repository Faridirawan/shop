@extends('layouts.template')

@section('content')
<div class="container-fluid">
    <!-- Basic Examples -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        DATA TRANSAKSI
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li>
                            <a href="{{route('transactions.create')}}">
                                <button type="button" class="btn btn-default waves-effect btn-sm"><i class="material-icons">add_circle</i></button>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                	<th>No</th>
                                    <th>No Transaksi</th>
                                    <th>Nama Anggota</th>
                                    <th>Nama Produk</th>
                                    <th>Kuantitas</th>
                                    <th>Diskon</th>
                                    <th>Total</th>
                                    <th>Pilihan</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>No Transaksi</th>
                                    <th>Nama Anggota</th>
                                    <th>Nama Produk</th>
                                    <th>Kuantitas</th>
                                    <th>Diskon</th>
                                    <th>Total</th>
                                    <th>Pilihan</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($transaction as $i => $item)
                                <tr>
                                    <td>{{$i+1}}</td>
                                    <td>{{$item->trx_number}}</td>
                                    <td>{{$item->categoryRef->full_name}}</td>
                                    <td>{{$item->categRef->name}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>Rp {{$item->discount}}</td>
                                    <td>Rp {{$item->total}}</td>
                                    <td>
                                        <form action="{{ route('transactions.destroy',$item->id) }}" method="post">
                                            <a href="{{ route('transactions.edit',$item->id) }}">
                                                <button type="button" class="btn btn-success btn-sm"><i class="material-icons">edit</i></button>
                                            </a>
                                            @csrf 
                                            @method('delete')
                                                <button type="submit" class="btn btn-danger btn-sm" 
                                                onclick="return confirm('Yakin ingin dihapus ?')"><i class="material-icons">delete</i></button>   
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Examples -->
</div>
@endsection