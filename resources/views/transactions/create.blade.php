@extends('layouts.template')

@section('content')
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Tambah Data Transaksi</h2>
            </div>
            <div class="body">
                <form action="{{ route('transactions.store') }}" method="POST">
                    @csrf
                    <div class="form-group form-float">
                        <label>Nama Anggota</label>
                        <select class="form-control show-tick" id="members" name="members">
                            <option value="">-- Please select --</option>
                                @foreach ($members as $item)
                                    <option value="{{ $item->id }}">{{ $item->full_name }}</option>
                                @endforeach
                        </select>
                    </div>
                    <div class="form-group form-float">
                        <label>Nama Produk</label>
                        <select class="form-control show-tick" id="products" name="products">
                            <option value="">-- Please select --</option>
                                @foreach ($products as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                        </select>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="number" class="form-control" id="quantity" name="quantity" required>
                            <label class="form-label">Kuantitas</label>
                        </div>
                    </div>
                    <button class="btn btn-danger waves-effect" type="submit">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
@endsection