@extends('layouts.template')

@section('content')
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Tambah Data Kategori</h2>
            </div>
            <div class="body">
                <form action="{{route('member_category.update', $category->id)}}" method="POST">
                    @csrf
                    @method('put')
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" id="name" name="name" value="{{ $category->name }}">
                            <label class="form-label">Nama Kategori</label>
                        </div>
                    </div>
                    <button class="btn btn-danger waves-effect" type="submit">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
@endsection