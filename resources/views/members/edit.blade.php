@extends('layouts.template')

@section('content')
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Edit Data Anggota</h2>
            </div>
            <div class="body">
                <form action="{{ route('members.update', $member->id) }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="form-group form-float">
                        <label>Kategori Anggota</label>
                        <select class="form-control show-tick" id="category" name="category">
                            <option value="">-- Please select --</option>
                                @foreach ($category as $item)
                                    <option value="{{ $item->id }}" {{ $member->member_category_id ==
                             $item->id ? 'selected' : ''}}>{{ $item->name }}</option>
                                @endforeach
                        </select>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" id="full_name" name="full_name" required value="{{ $member->full_name }}">
                            <label class="form-label">Nama Lengkap</label>
                        </div>
                    </div>
                    <label class="form-label">Tanggal Lahir</label>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="date" class="form-control" id="dob" name="dob" value="{{ $member->dob }}">
                        </div>
                    </div>         
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea name="address" id="address" cols="30" rows="5" class="form-control no-resize" required>{{ $member->address }}</textarea>
                            <label class="form-label">Alamat</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <label>Gender</label><br>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" name="gender" id="gender1" value="laki-laki" class="with-gap radio-col-red"
                            {{ $member->gender == 'laki-laki' ? 'checked' : '' }}>
                            <label class="custom-control-label" for="gender1">Laki-Laki</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" name="gender" id="gender2" value="perempuan" class="with-gap radio-col-red"
                            {{ $member->gender == 'perempuan' ? 'checked' : '' }}>
                            <label class="custom-control-label" for="gender2">Perempuan</label>
                        </div>
                    </div>
                    <button class="btn btn-danger waves-effect" type="submit">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
@endsection