<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Member;
use App\Product;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaction = Transaction::with('categoryRef','categRef')->get();
        $products = Product::all();
        $members = Member::all();
        return view('transactions.index', compact('members','products','transaction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $transaction = Transaction::all();
        $members = Member::all();
        $products = Product::all();
        return view('transactions.create', compact('members','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $trx_number = str_replace("-", "",date("Y-m-d")).substr(str_shuffle('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
        $discountrate = Member::where('id', $request->members)->with('categoryRef')->first()->categoryRef->discount;
        $price = Product::where('id', $request->products)->first()->price;
        $discount = $price * $request->quantity * $discountrate / 100;
        $total = $price * $request->quantity - $discount;

        $transaction = new Transaction;
        $transaction->trx_number = $trx_number;
        $transaction->member_id = $request->members;
        $transaction->product_id = $request->products;
        $transaction->quantity = $request->quantity;
        $transaction->discount = $discount;
        $transaction->total = $total;
        $transaction->save();
        return redirect()->route('transactions.index');    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        $members = Member::all();
        $products = Product::all();
        return view('transactions.edit', compact('members','products','transaction'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $trx_number = str_replace("", "",date("Y-m-d")).substr(str_shuffle('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
        $discountrate = Member::where('id', $request->members)->with('categoryRef')->first()->categoryRef->discount;
        $price = Product::where('id', $request->products)->first()->price;
        $discount = $price * $request->quantity * $discountrate / 100;
        $total = $price * $request->quantity - $discount;

        $transaction->trx_number = $trx_number;
        $transaction->member_id = $request->members;
        $transaction->product_id = $request->products;
        $transaction->quantity = $request->quantity;
        $transaction->discount = $discount;
        $transaction->total = $total;
        $transaction->update();
        return redirect()->route('transactions.index');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        $transaction->delete();
        return redirect()->route('transactions.index'); 
    }
}
