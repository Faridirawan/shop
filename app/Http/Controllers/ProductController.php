<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('categoryRef')->get();
        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = ProductCategory::all();
        return view('product.create', compact('category'));    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $extension = $request->image->extension();
        $namafile = str_replace("","",date("Y-m-d")).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
        $request->image->move(public_path('/products/gambar'), $namafile.'.'.$extension);

        $product = new Product;
        $product->product_category_id = $request->category;
        $product->name = $request->name;
        $product->image = $namafile.'.'.$extension;
        $product->desc = $request->desc;
        $product->price = $request->price;
        $product->save();
        return redirect()->route('product.index');    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $product = Product::with('pembeli.categoryRef')->findOrFail($product->id);
        return view('product.history', compact('product'));    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $category = ProductCategory::all();
        return view('product.edit', compact('category','product'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $extension = $request->image->extension();
        $namafile = str_replace("","",date("Y-m-d")).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
        $request->image->move(public_path('/products/gambar'), $namafile.'.'.$extension);
        
        $products->product_category_id = $request->category;
        $products->name = $request->name;
        $products->image = $namafile.'.'.$extension;
        $products->desc = $request->desc;
        $products->price = $request->price;
        $products->update();
        return redirect()->route('product.index');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('product.index');    
    }
}
